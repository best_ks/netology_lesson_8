var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

//Задание 1. Создание массива students[].
var students=[];
studentsAndPoints.forEach(function(item,i,arr){
	if(!(i%2))
		students.push({name: arr[i], points: arr[i+1]});
});

function showFunc(){
	console.log('Студент %s набрал %d баллов',this.name,this.points);
}
//Добавление метода show к объектам в массиве
students.forEach(function(item){
	item.show = showFunc;
});

students.forEach(function(item){
    item.show();
});

//Задание 2. Добавление в список новых студентов.
students.push({name: 'Николай Фролов', points: 0, show: showFunc},
		{name: 'Олег Боровой', points: 0, show: showFunc});

console.log('\nДобавлены студенты:');
students.forEach(function(item){
	item.show();
});

//Задание 3. Увеличиваем баллы студентам
students.forEach(function(item){
	if(item.name=='Николай Фролов')
		item.points+=10;
	if(item.name=='Ирина Овчинникова'||item.name=='Александр Малов')
		item.points+=30;
});

console.log('\nУвеличены баллы студентам:');
students.forEach(function(item){
	item.show();
});

//Задание 4. Список студентов с 30 и более баллами
console.log('\nСписок студентов с 30 и более баллами:');
students.forEach(function(item){
	if(item.points>=30)
		item.show();
});

//Задание 5. Добавить поле worksAmount
students.forEach(function(item){
	item.worksAmount = works(item);
});

function works(item){
	return item.points/10;
}

console.log('Добавлено поле worksAmount:');
students.forEach(function(item){
	item.show();
	console.log('worksAmount: %d',item.worksAmount);
});

//Дополнительное задание
students.findByName = function(str){
	return students.find(function(item){
        return item.name == str;
    });
}

console.log('\nДополнительное задание:');
var st = students.findByName('Семен Слепаков');
if(st){
	st.show = showFunc;
	st.show();
}
else
	console.log('Студент не найден');